#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

#ifndef BEHAVE_H
#define BEHAVE_H

namespace behave{
  class Behave{

    public:

      Behave();
      int max_beams;
      double straight_prob;
      double right_prob;
      double left_prob;

      ros::Publisher vel_pub_;
      ros::Subscriber laser_sub;
      geometry_msgs::Twist cmd_vel_recover;
      geometry_msgs::Twist cmd_vel_go_straight;
      geometry_msgs::Twist cmd_vel_turn_right;
      geometry_msgs::Twist cmd_vel_turn_left;

      void laser_Callback(const sensor_msgs::LaserScanConstPtr& laser_scan);
  };
}

#endif // BEHAVE_H
